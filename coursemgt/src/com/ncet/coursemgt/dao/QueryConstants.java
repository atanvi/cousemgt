package com.ncet.coursemgt.dao;

public interface QueryConstants {
	
	String ADD_NEW_INSTRUCTOR="insert into instructor(name,email,yearofexp,status) values(?,?,?,?)";
	String SEARCH_INSTRUCTOR="select instid, name, email, yearofexp, status from instructor where name = ? and email = ? ";
}
