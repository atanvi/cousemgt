package com.ncet.coursemgt.service;

import java.util.List;

import com.ncet.coursemgt.dao.CourseMgtDao;
import com.ncet.coursemgt.dao.CourseMgtDaoImpl;
import com.ncet.coursemgt.domain.Course;
import com.ncet.coursemgt.domain.Instructor;
import com.ncet.coursemgt.domain.Student;
import com.ncet.coursemgt.reports.CourseReport;

public class CourseMgtServiceImpl implements CourseMgtService{

	private static CourseMgtService courseMgtService;
	private CourseMgtDao courseMgtDao;
	
	private  CourseMgtServiceImpl() {
		courseMgtDao = CourseMgtDaoImpl.getInstanceCourseDaoImpl();
	}
	
	public static CourseMgtService getInstanceCouserMgtService(){
		synchronized (CourseMgtServiceImpl.class) {
			if(courseMgtService == null)
				courseMgtService = new CourseMgtServiceImpl();
		}
		return courseMgtService;
	}
	
	@Override
	public Instructor findInstructor(String name, String email) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public String addInstructor(Instructor instructor) {
		
		return null;
	}

	@Override
	public List<Instructor> getActiveInstrctors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Instructor updateInstructor(Instructor updatedValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String addCourse(Course course) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Course updateCourse(Course course) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Course> getAllActiveCourses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String registerStudent(Student student) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Student> getAllStudents(String courseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean registerStudentForCourse(int stuId, int courId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<CourseReport> getAllActiveCourseDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
