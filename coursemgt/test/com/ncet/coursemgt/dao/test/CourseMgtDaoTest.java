package com.ncet.coursemgt.dao.test;

import org.junit.Assert;
import org.junit.Test;

import com.ncet.coursemgt.dao.CourseMgtDao;
import com.ncet.coursemgt.dao.CourseMgtDaoImpl;
import com.ncet.coursemgt.domain.Instructor;
import com.ncet.coursemgt.domain.STATUS;

public class CourseMgtDaoTest {

	@Test
	public void addInstructor() {
		CourseMgtDao courseMgtDao = CourseMgtDaoImpl.getInstanceCourseDaoImpl();
		Instructor instructor=new Instructor();
		instructor.setName("Krish");
		instructor.setEmail("krish.t@mail.com");
		instructor.setYearsExp(5);
		instructor.setStatus(STATUS.ACTIVE);
		Assert.assertEquals("Krish", courseMgtDao.addInstructor(instructor));
		
	}

}
