package com.ncet.coursemgt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.ncet.coursemgt.dao.util.DbUtil;
import com.ncet.coursemgt.domain.Course;
import com.ncet.coursemgt.domain.Instructor;
import com.ncet.coursemgt.domain.Student;
import com.ncet.coursemgt.reports.CourseReport;
import static com.ncet.coursemgt.dao.QueryConstants.*;
public class CourseMgtDaoImpl implements CourseMgtDao {

	private static CourseMgtDaoImpl courseMgtDaoImpl;
	private DbUtil dbUtil ;
	
	private CourseMgtDaoImpl() {
			dbUtil = DbUtil.obj;
	}

	public static CourseMgtDaoImpl getInstanceCourseDaoImpl() {
		synchronized (CourseMgtDaoImpl.class) {
			if (courseMgtDaoImpl == null)
				courseMgtDaoImpl = new CourseMgtDaoImpl();
		}
		return courseMgtDaoImpl;
	}

	@Override
	public String addInstructor(Instructor instructor) {
		Connection con=null;
		PreparedStatement pst=null;
		try{
			con = dbUtil.getConnection();
			pst = con.prepareStatement(ADD_NEW_INSTRUCTOR);
			pst.setString(1, instructor.getName());
			pst.setString(2, instructor.getEmail());
			pst.setInt(3, instructor.getYearsExp());
			pst.setString(4, instructor.getStatus().name());
			int isExecuted = pst.executeUpdate();
			if(isExecuted != 0)
				return instructor.getName();
		}catch(SQLException e){
			System.out.println("While adding new instrctor : "+e);
		}
		return null;
	}

	@Override
	public List<Instructor> getActiveInstrctors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Instructor updateInstructor(Instructor updatedValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String addCourse(Course course) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Course updateCourse(Course course) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Course> getAllActiveCourses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String registerStudent(Student student) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Student> getAllStudents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Student> getAllStudents(String courseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean registerStudentForCourse(int stuId, int courId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<CourseReport> getAllActiveCourseDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
