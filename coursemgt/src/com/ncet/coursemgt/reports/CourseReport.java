package com.ncet.coursemgt.reports;

public class CourseReport {
		
		private String courId;
		private String courseTitle;
		private String facultyName;
		private String studentName;
		public String getCourId() {
			return courId;
		}
		public void setCourId(String courId) {
			this.courId = courId;
		}
		public String getCourseTitle() {
			return courseTitle;
		}
		public void setCourseTitle(String courseTitle) {
			this.courseTitle = courseTitle;
		}
		public String getFacultyName() {
			return facultyName;
		}
		public void setFacultyName(String facultyName) {
			this.facultyName = facultyName;
		}
		public String getStudentName() {
			return studentName;
		}
		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}
		
		
		
}
